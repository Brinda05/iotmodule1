# Industrial IOT

## Evolution of Industry

- **Industry 1.0(1784):-** Mechanisation, Steam power & Weaving loom.

- **Industry 2.0(1870):-** Mass Production, Assembly line & Electrical energy.

- **Industry 3.0 (1969):** Automation, Computers & Electronics.
   In Industry 3.0, data is stored in databases and represented in excel.

   ![EVOLUTION](download.jpg)

# Industry 3.0 architecture:-
 
![INDUSTRY 3.0](industry3.0.png)

-_Field devices_ can be actuators (positioning elements, valve and damper drives, and frequency converters) and sensors (measuring transducers, probes, and monitors). 

-_Control devices_ are used to "turn on" or "turn off" current flow in an electrical circuit. Example:- CNC,PLC,DCS.

-_stations_ comprises of both field device and control device.

-_Work centres_ is set of stations.Example:-SCADA, HISTORIAN

-_Enterprise_ is set of factories
- Enterprise Resource Planning
- Manufacturing Execution System (MES)

- The bridge between the field devices and the control devices is called Field Bus.

- **Communication protocols:** 
		 
		 Field Buses use several communication protocols like Modbus, CanOpen, EtherCAT, etc., are optimized for sending data to a central server inside a factory.


# **Industry 4.0(today)**

Industry4.0 is industry 3.o connected to internet .The main purpose of connected devices to the internet is the ability to send and recieve data.

- Use of data obtained:-

**Dashboards:** It displays vivid graphic displays of live data cllected 
directly from the factory floor.

**Remote web scada:**  Enables users to remotely monitor & control remote devices.

- Remote control configuration of devices.

- Real time event stream processing.

- Analytics with predictive models.

- Real time alerts and alarms.

-Automated device provisioning (Auto discovery).

#### Industry 4.0 architecture:-

![Industry 4.0 Architecture](industry-4.png)
>**Communication protocols:** 
		
		`MQTT.org`, `AMQP`, `OPC UA`, `CoAP RFC 7252`, `HTTP`, `WebSockets`, `REST*ful* API`, etc., are optimized for sending data to cloud for data analysis.

#### Challenges faced by Industry 4.0:-
 
-**Cost:** They are expensive.

-**Downtime:** Changing hardware takes a lot of time.

-**Reliability:** Some devices might be unproven or unreliable to invest in.

### The solution:

Get data from Industry 3.0 devices/meters/sensors without changes to the original device.  And implement it to Industry 4.0 devices to send data to the cloud.

## Getting data from Industry 3.0 device

>Convert Industry 3.0 protocols into Industry 4.0 protocols.

These conversions can be done using libraries available in Industry 3.0 and link it to Industry 4.0.

![Instances](instances.png)

### Challenges in conversion:

- Expensive hardware.
- Lack of documentation.
- Proprietary PLC protocols.

## Roadmap for an IOT project:-
1. Identify most popular Industry 3.0 devices.
2. Study protocols that these devices communicate.
3. Get data from the Industry 3.0 device.
4. Send the data to cloud for Industry 4.0 device.

### Analysing data once it is sent to internet:

- **IoT TSDB Tools**

 	These tools are used to store our data in the form of Time Series Databases. In a very basic sense, Time series data are simply measurements or events that are tracked, monitored, downsampled, and aggregated over time. This could be server metrics, application performance monitoring, network data, sensor data, events, clicks, trades in a market, and many other types of analytics data.

	*For example, Prometheus, InfluxDB, etc.*

- **IoT Dashboards**

	Dashboards allow us to view our data in beautiful dashboards.

	*For example, Grafana, Thingsboard, etc.*

 - **IoT Platforms**

	Platforms allow us to analyse our data.

	*For example, AWS IoT, Google IoT, Azure IoT, Thingsboard, etc.*

 - **Recieve Alerts**

	Zaiper, Twilio, etc., allow us to get alerts based on our data using these platforms.













